FROM node:18 as build-stage
WORKDIR /app
COPY yarn.lock package.json ./
RUN yarn install
COPY ./ /app/

RUN yarn build
#COPY dist /dist

FROM nginx:1.15
COPY --from=build-stage /app /usr/share/nginx/html
#RUN mv /usr/share/nginx/html/build /usr/share/nginx/html/dist/

COPY  /default.conf /etc/nginx/conf.d/default.conf
