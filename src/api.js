import { getCookie, setCookie } from "./helpers";

const BASE_URL = import.meta.env.VITE_BASE_URL

export const fetchContracts = async (params) => {
  const strParams = new URLSearchParams(params).toString();
  const response = await fetch(`${BASE_URL}/contract/?${strParams}`);
  const data = await response.json();
  return data;
};

export const fetchContract = async (id) => {
  const response = await fetch(`${BASE_URL}/contract/${id}/`);
  const data = await response.json();
  return data;
}

export const fetchProducts = async (params) => {
  const strParams = new URLSearchParams(params).toString();
  const response = await fetch(`${BASE_URL}/warehouse/product/?${strParams}`);
  const data = await response.json();
  return data;
};

export const fetchProduct = async (id) => {
  const response = await fetch(`${BASE_URL}/warehouse/product/${id}/`);
  const data = await response.json();
  return data;
}

export const fetchRemains = async (id) => {
  const response = await fetch(`${BASE_URL}/warehouse/product/${id}/remains/`);
  const data = await response.json();
  return data;
}

export const sendLogin = async (params) => {
  const response = await fetch(`${BASE_URL}/garpix_user/login/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(params)
  });
  const data = await response.json();
  setCookie('access_token', data.access_token)
  return data;
}

export const fetchCurrentUser = async () => {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'authorization': `Bearer ${getCookie('access_token')}`
  };

  const response = await fetch(
    `${BASE_URL}/user/current/`,
    {
      headers: {
        ...headers
      }
    }
  );
  const data = await response.json();
  return data;
}

export const fetchMessages = async ({ page }) => {
  const strParams = new URLSearchParams({page}).toString();
  const response = await fetch(`${BASE_URL}/chat/message/?${strParams}`, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': `Bearer ${getCookie('access_token')}`
    }
  });
  const data = await response.json();
  data.results = data.results.reverse();
  return data;
}

export const sendMessage = async (params) => {
  const response = await fetch(`${BASE_URL}/chat/message/send/`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': `Bearer ${getCookie('access_token')}`
    },
    body: JSON.stringify(params)
  });
  const data = await response.json();
  return data;
}

export const fetchUsers = async (params) => {
  const strParams = new URLSearchParams(params).toString();
  const response = await fetch(`${BASE_URL}/user/?${strParams}`, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': `Bearer ${getCookie('access_token')}`
    }
  });
  const data = await response.json();
  return data;
}

export const createUser = async (params) => {
  const response = await fetch(`${BASE_URL}/user/add_user/`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': `Bearer ${getCookie('access_token')}`
    },
    body: JSON.stringify(params)
  });
  const data = await response.json();
  return data;
}

export const sendCalculation = async (params) => {
  const response = await fetch(`${BASE_URL}/chat/message/calculation/`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': `Bearer ${getCookie('access_token')}`
    },
    body: JSON.stringify(params)
  });
  const data = await response.json();
  return data;
}

export const fetchCalculations = async (params) => {
  const strParams = new URLSearchParams(params).toString();
  const response = await fetch(`${BASE_URL}/chat/calculation/?${strParams}`, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': `Bearer ${getCookie('access_token')}`
    }
  });
  const data = await response.json();
  return data;
}

export const sendFile = async (fd) => {
  const response = await fetch(`${BASE_URL}/user/load_file/`, {
    method: 'POST',
    headers: {
      // 'Accept': 'application/json',
      // 'Content-Type': 'application/json',
      'authorization': `Bearer ${getCookie('access_token')}`
    },
    body: fd
  });
  const data = await response.json();
  return data;
}