import { writable } from 'svelte/store';
import { fetchCurrentUser } from '../api';

export const user = writable(null);
