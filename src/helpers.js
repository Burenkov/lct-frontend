export const createPages = (current, max) => {
  if (!current || !max) return []

  const items = [1]

  if (current === 1 && max === 1) return items
  if (current > 4) items.push('...')

  const r = 2;
  const r1 = current - r;
  const r2 = current + r;

  for (let i = r1 > 2 ? r1 : 2; i <= Math.min(max, r2); i++) items.push(i)

  if (r2 + 1 < max) items.push('...')
  if (r2 < max) items.push(max)

  return items
};

export const pagination = (activePage, count, pageSize, makePages) => {
  const _count = Math.ceil(count / pageSize)
  const total = isNaN(_count) ? 0 : _count;
  const funcPages = makePages ? makePages : createPages;
  return {
    pages: funcPages(activePage, total),
    currentPage: activePage,
    hasNextPage: activePage < total,
    hasPreviousPage: activePage > 1,
    previousPage: activePage - 1,
    nextPage: activePage + 1,
    totalPages: total,
  }
}

export function setCookie(name, value, days = 365) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

export function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

export function parseQuery(queryString) {
  let query = {};
  let pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
  for (let i = 0; i < pairs.length; i++) {
    let pair = pairs[i].split('=');
    query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
  }
  return query;
}